<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelcomePageController extends Controller
{
    public function index()
    {
        return view('welcome');
    }
    
    public function apiDocumentation()
    {
        return File::get(public_path() . '/docs/api/index.html');
    }
}
