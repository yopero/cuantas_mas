<?php declare (strict_types = 1);
namespace Dgarrido\CuantasMasApi\Formatters;

use Dgarrido\CuantasMasApi\Results\Result;

class VictimsFormatter
{
    public function getVictimData(array $currentRecord): Result
    {
        $victimData = [
            'name' => $currentRecord["Nombre de la víctima"],
            'lastname' => $currentRecord["Nombre de la víctima"],
            'age' => (int)$currentRecord["Edad de la víctima"],
            'previous_aggresions' => $currentRecord["Agresión previa"],
            'children' => (int)$currentRecord["Número de hijos"],
            'cause_of_death' => $currentRecord["Tipo de agresión que causó la muerte"],
        ];

        return new Result('Ok', [$victimData]);
    }
}