<?php declare (strict_types = 1);
namespace Dgarrido\CuantasMasApi\Formatters;

use Dgarrido\CuantasMasApi\Results\Result;
use Dgarrido\CuantasMasApi\Formatters\FemicideObject;
use Dgarrido\CuantasMasApi\Formatters\LocationsFormatter;
use Dgarrido\CuantasMasApi\Formatters\AssailantsFormatter;
use Dgarrido\CuantasMasApi\Formatters\FemicidesDataFormatter;
use Dgarrido\CuantasMasApi\Formatters\VictimsFormatter;
use Dgarrido\CuantasMasApi\Formatters\PressReleasesFormatter;

class FemicideFormatter
{
    protected $currentRecord;
    protected $currentIndex;

    public function __construct(
        LocationsFormatter $locationsFormatter, 
        AssailantsFormatter $assailantsFormatter,
        FemicidesDataFormatter $femicidesDataFormatter,
        VictimsFormatter $victimsFormatter,
        PressReleasesFormatter $pressReleasesFormatter,
        TsjCasesFormatter $tsjCasesFormatter
        )
    {
        $this->locationsFormatter = $locationsFormatter;
        $this->assailantsFormatter = $assailantsFormatter;
        $this->femicidesDataFormatter = $femicidesDataFormatter;
        $this->victimsFormatter = $victimsFormatter;
        $this->pressReleasesFormatter = $pressReleasesFormatter;
        $this->tsjCasesFormatter = $tsjCasesFormatter;
    }

    public function formatRow(array $record, int $currentRow): FemicideObject
    {
        $headerOffset = 1;
        $this->currentIndex = $currentRow + $headerOffset;

        $this->currentRecord = $record;

        $object = new FemicideObject($this->getFemicidesData());

        return $object;
    }

    private function getFemicidesData(): array
    {

        $femicideMetaData = $this->femicidesDataFormatter->getFemicideMetaData($this->currentRecord);
        if($femicideMetaData->getCode() == 'Ok'){
            $femicidesData["femicide"] = $femicideMetaData->getData();
        }
        
        $assailantData = $this->assailantsFormatter->getAssailantData($this->currentRecord);
        if($assailantData->getCode() == 'Ok'){
            $femicidesData["assailant"] = $assailantData->getData();
        }

        $locationData = $this->locationsFormatter->getLocationData($this->currentRecord);
        if($locationData->getCode() == 'Ok'){
            $femicidesData["location"] = $locationData->getData();
        }

        $victimData = $this->victimsFormatter->getVictimData($this->currentRecord);
        if($victimData->getCode() == 'Ok'){
            $femicidesData["victim"] = $victimData->getData();
        }

        $pressReleasesData = $this->pressReleasesFormatter->getPressReleasesData($this->currentRecord);
        if($pressReleasesData->getCode() == 'Ok'){
            $femicidesData["pressReleases"] = $pressReleasesData->getData();
        }

        $tsjCaseData = $this->tsjCasesFormatter->getTsjCaseData($this->currentRecord);
        if($tsjCaseData->getCode() == 'Ok'){
            $femicidesData["tsjCase"] = $tsjCaseData->getData();
        }
        
        return $femicidesData;  
    }
}