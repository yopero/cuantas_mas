<?php declare (strict_types = 1);
namespace Dgarrido\CuantasMasApi\Formatters;
use Dgarrido\CuantasMasApi\Results\Result;
class AssailantsFormatter
{
    public function getAssailantData(array $currentRecord) : Result
    {
        $assailantData = [
            'name' => $currentRecord["Nombre del autor/Presunto autor"],
            'lastname' => $currentRecord["Nombre del autor/Presunto autor"],
            'age' => $this->helperGetAge($currentRecord["Edad del acusado"])->getData(),
            'temperance' => $currentRecord["Temperancia"],
            'suicide_attempt' => $this->getBooleanSuicideAttempt($currentRecord["Intento de suicidio"])->getData(),
            'current_situation' => $currentRecord["Situación del presunto autor"],
            'relation_to_victim' => $currentRecord["Relación con la víctima"],
        ];

        return new Result('Ok',[$assailantData]);
    }

    private function helperGetAge(string $number): Result
    {
        if ((int)$number != 0) {
            return new Result('Ok', [(int)$number]);
        }
        return new Result('not_number', [null]);
    }

    private function getBooleanSuicideAttempt(string $value): Result //todo find what to return
    {
        $suicideAttempt = $value;
        switch (strtolower($suicideAttempt)) {
            case "no":
                return new Result('Ok', [false]);
            case "si":
                return new Result('Ok', [true]);
            default:
                return new Result('not_valid_boolean', [null]);
        }
    }

}

