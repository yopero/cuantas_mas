<?php declare (strict_types = 1);
namespace Dgarrido\CuantasMasApi\Formatters;

use Dgarrido\CuantasMasApi\Results\Result;

class PressReleasesFormatter
{
    public function getPressReleasesData(array $currentRecord): Result
    {
        $pressReleasesData = [];
        $pressReleasesSources = [
            ['news' => 'Nota de Prensa 1'],
            ['news' => 'Nota de Prensa 2'],
            ['news' => 'Nota de Prensa 3'],
            ['news' => 'Nota de Prensa 4'],
            ['video' => 'Youtube 1'],
            ['video' => 'Youtube 2'],
            ['other' => 'Otros formatos 1'],
            ['other' => 'Otros formatos 2'],
            ['other' => 'Otros formatos 3']
        ];
        foreach ($pressReleasesSources as $source) {
            foreach ($source as $mediaType => $pressReleaseDescription) {
                if ($currentRecord[$pressReleaseDescription]) {
                    array_push(
                        $pressReleasesData,
                        ['media_type' => $mediaType, 'link' => $currentRecord[$pressReleaseDescription]]
                    );
                }
            }
        }

        return new Result('Ok', [$pressReleasesData]);
    }
}