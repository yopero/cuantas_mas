<?php declare (strict_types = 1);
namespace Dgarrido\CuantasMasApi\Formatters;

use Dgarrido\CuantasMasApi\Results\Result;

class TsjCasesFormatter
{
    public function getTsjCaseData(): Result
    {
        return new Result('NotOk');
    }
}