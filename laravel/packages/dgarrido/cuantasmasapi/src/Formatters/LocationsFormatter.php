<?php declare (strict_types = 1);
namespace Dgarrido\CuantasMasApi\Formatters;

use Dgarrido\CuantasMasApi\Results\Result;


class LocationsFormatter
{
    public function getLocationData(array $currentRecord): Result
    {
        $coords = $currentRecord["Geolocalización"];
    
        $coords = $this->getLatLng($coords);
    
        $locationData = [
            'town' => $currentRecord["Lugar del feminicidio"],
            'province' => $currentRecord["Provincia"],
            'department' => $currentRecord["Departamento"],
            'lat' => $coords[0],
            'lng' => $coords[1],
        ];
    
        return new Result('Ok',[$locationData]);
    }
    
    private function getLatLng(string $geolocation): array
    {
        $coords = explode(", ", $geolocation);

        if ($this->coordsHasTwoElements($coords)) {
            return $this->castToFloat($coords);
        }
    
        return [0, 0];
    }

    private function coordsHasTwoElements(array $coords): bool
    {
        if(count($coords) == 2){
            return true;
        }
        return false;
    }

    private function castToFloat(array $collection): array
    {
        foreach($collection as $index => $item)
        {
            $collection[$index] = (float)$item;
        }
        
        return $collection;
    }

}

