<?php declare (strict_types = 1);
namespace Dgarrido\CuantasMasApi\Formatters;

class FemicideObject
{
    protected $femicide;
    protected $victim;
    protected $assailant;
    protected $pressReleases;
    protected $tsjCase;
    protected $location;

    public function __construct(array $femicide)
    {
        $this->initialize($femicide);
    }
    
    private function initialize(array $femicide)
    {
        $femicideComponents = ['femicide', 'victim', 'assailant', 'pressReleases', 'tsjCase', 'location'];
        foreach($femicideComponents as $component){
            if(array_key_exists($component, $femicide)){
                $this->$component = $femicide[$component];
            } else {
                $this->$component = [];
            }
        }
    }

    public function getFemicide() : array
    {
        return $this->femicide;
    }

    public function getVictim() : array
    {
        return $this->victim;
    }

    public function getAssailant() : array
    {
        return $this->assailant;
    }

    public function getPressReleases() : array
    {
        return $this->pressReleases;
    }

    public function getTsjCase() : array
    {
        return $this->tsjCase;
    }

    public function getLocation() : array
    {
        return $this->location;
    }
}