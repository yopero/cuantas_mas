<?php declare (strict_types = 1);
namespace Dgarrido\CuantasMasApi\Formatters;

use Dgarrido\CuantasMasApi\Results\Result;

class FemicidesDataFormatter
{
    public function getFemicideMetaData(array $currentRecord) : Result
    {
        $femicideData = [
            'circumstance' => $currentRecord["Circunstancias"],
            'notes' => $currentRecord["Observaciones"],
            'status' => $currentRecord["Estado del caso"],
        ];
        
        $date = $this->getFormattedDate($currentRecord["Fecha del feminicidio"]);
        if($date->getCode() == 'Ok'){
            $femicideData['date'] = $date->getData();
        }

        return new Result('Ok', [$femicideData]);
    }

    private function getFormattedDate(string $date)
    {
        $unixTime = strtotime($date);

        if (!$unixTime) {
            //Log here
            return new Result('invalid_date_format', [$date]);
        }

        return new Result('Ok', [date('Y-m-d', strtotime($date))]);
    }
}

