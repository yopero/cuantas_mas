<?php declare (strict_types=1);
namespace Dgarrido\CuantasMasApi\Repository;

use Illuminate\Database\Eloquent\Model;
use Dgarrido\CuantasMasApi\Repository\RepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Dgarrido\CuantasMasApi\Results\Result;
use Dgarrido\CuantasMasApi\Formatters\FemicideObject;
use Dgarrido\CuantasMasApi\Models\Femicide;
use Dgarrido\CuantasMasApi\Models\Location;
use Dgarrido\CuantasMasApi\Repository\RepositoryHelpersTrait;
use Dgarrido\CuantasMasApi\Models\TsjCase;

class FemicidesRepository implements RepositoryInterface
{
    use RepositoryHelpersTrait;
    /**
     * Bespoke Repository for the Femicide model needed for getting relationships.
     *
     * @var Model
     */
    protected $model;

    protected $femicideId;

    /**
     * FemicidesRepository constructor.
     *
     * @param Model $model
     */
    public function __construct(Model $model, Model $location, Model $victim, Model $assailant, Model $pressReleases, Model $tsjCase)
    {
        $this->model = $model;
        $this->locationsRepository = new LocationsRepository($location);
        $this->victimsRepository = new VictimsRepository($victim);
        $this->assailantsRepository = new AssailantsRepository($assailant);
        $this->pressReleasesRepository = new PressReleasesRepository($pressReleases);
        $this->tsjCasesRepository = new TsjCasesRepository($tsjCase);
    }

    public function save(FemicideObject $femicideObject): void
    {
        $this->saveFemicideData($femicideObject->getFemicide());
        $this->locationsRepository->save($femicideObject->getLocation(), $this->femicideId);
        $this->victimsRepository->save($femicideObject->getVictim(), $this->femicideId);
        $this->assailantsRepository->save($femicideObject->getAssailant(), $this->femicideId);
        $this->pressReleasesRepository->save($femicideObject->getPressReleases(), $this->femicideId);
        $this->tsjCasesRepository->save($femicideObject->getTsjCase(), $this->femicideId);
    }

    private function saveFemicideData(array $femicideData): void
    {
        $femicide = new Femicide();
        $femicideData = array_merge($femicideData, $this->timestamps());

        $femicide->fill($femicideData);
        $femicide->save();

        $this->femicideId = $femicide->id;
    }

    /**
     * Returns all femicides.
     *
     * @return Result
     */
    public function index(): Result
    {
        return new Result("Ok", [$this->model->paginate()]);
    }

    /**
     * Returns a single femicide.
     *
     * @param string $id
     * @return Result
     */
    public function show(string $id): Result
    {
        $item = $this->model->find($id);

        if(! $item){
            return new Result("not_found");
        }
        return new Result("Ok", [$item]);
    }
}
