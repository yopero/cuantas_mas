<?php declare (strict_types = 1);
namespace Dgarrido\CuantasMasApi\Repository;
use Dgarrido\CuantasMasApi\Models\Victim;

class VictimsRepository extends Repository
{
    public function save(array $victimData, int $femicideId):void
    {
        $victim = new Victim();
        $victimData['femicideId'] = $femicideId;
        $victim->fill($victimData);
        $victim->save();
    }
}