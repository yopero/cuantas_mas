<?php declare (strict_types = 1);
namespace Dgarrido\CuantasMasApi\Repository;
use Dgarrido\CuantasMasApi\Models\Assailant;

class AssailantsRepository extends Repository
{
    public function save(array $assailantData, int $femicideId): void
    {
        $assailant = new Assailant();
        $assailantData['femicideId'] = $femicideId;
        $assailant->fill($assailantData);
        $assailant->save();
    }
}