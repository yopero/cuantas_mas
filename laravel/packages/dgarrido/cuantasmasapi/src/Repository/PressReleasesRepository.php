<?php declare (strict_types = 1);
namespace Dgarrido\CuantasMasApi\Repository;
use Dgarrido\CuantasMasApi\Models\PressRelease;

class PressReleasesRepository extends Repository
{
    public function save(array $pressReleasesData, int $femicideId): void
    {
        foreach($pressReleasesData as $pressReleaseData){
            $pressRelease = new PressRelease();
            $pressReleaseData['femicideId'] = $femicideId;
            $pressRelease->fill($pressReleaseData);
            $pressRelease->save();
        }
    }
}