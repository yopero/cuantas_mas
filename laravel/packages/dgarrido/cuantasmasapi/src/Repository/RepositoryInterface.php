<?php declare (strict_types=1);
namespace Dgarrido\CuantasMasApi\Repository;

use Dgarrido\CuantasMasApi\Results\Result;

interface RepositoryInterface 
{
    public function index(): Result;
    
    public function show(string $id): Result;
}