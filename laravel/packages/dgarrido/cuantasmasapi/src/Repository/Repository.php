<?php declare (strict_types=1);
namespace Dgarrido\CuantasMasApi\Repository;

use Illuminate\Database\Eloquent\Model;
use Dgarrido\CuantasMasApi\Repository\RepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Dgarrido\CuantasMasApi\Results\Result;

class Repository implements RepositoryInterface
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function index(): Result
    {
        return new Result("Ok", [$this->model->paginate(10)]);
    }

    public function show(string $id): Result
    {
        $item = $this->model->find($id);
        if(! $item){
            return new Result("not_found");
        }
        return new Result("Ok", [$item]);
    }


    /**
     * Returns all femicides.
     *
     * @return Result
     */
    public function indexAll(): Result
    {
        $count = count($this->model->all());
        return new Result("Ok", [$this->model->paginate($count)]);
    }
}
