<?php declare (strict_types = 1);
namespace Dgarrido\CuantasMasApi\Repository;

trait RepositoryHelpersTrait
{
    public function timestamps(): array
    {
        return [
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ];
    }
}