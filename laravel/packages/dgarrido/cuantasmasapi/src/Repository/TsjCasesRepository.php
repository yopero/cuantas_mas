<?php declare (strict_types = 1);
namespace Dgarrido\CuantasMasApi\Repository;
use Dgarrido\CuantasMasApi\Models\TsjCase;

class TsjCasesRepository extends Repository
{
    public function save(array $tsjCaseData, int $femicideId):void
    {
        $tsjCase = new TsjCase();
        $tsjCaseData['femicideId'] = $femicideId;
        $tsjCase->fill($tsjCaseData);
        $tsjCase->save();
    }
}