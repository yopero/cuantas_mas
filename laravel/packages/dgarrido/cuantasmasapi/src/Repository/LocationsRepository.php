<?php declare (strict_types = 1);
namespace Dgarrido\CuantasMasApi\Repository;
use Dgarrido\CuantasMasApi\Models\Location;

class LocationsRepository extends Repository
{
    public function save(array $locationData, int $femicideId): void
    {
        $location = new Location();
        $locationData['femicideId'] = $femicideId;
        $location->fill($locationData);
        $location->save();
    }
}