<?php declare (strict_types=1);
namespace Dgarrido\CuantasMasApi\Transformers;

trait Hateoas
{
    /**
     * Generate links for the item(s)(Hateoas).
     *
     * @param string $resourceName
     * @param integer $id
     * @return array
     */
    public function getLinks(string $resourceName, int $id): array
    {
        return [
            'links' => [
                'self'  => config("cmas::settings.api_url") . $resourceName . "/" . $id,
                'index' => config("cmas::settings.api_url") . $resourceName . "/",
            ],
        ];
    }
}