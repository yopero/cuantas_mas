<?php declare (strict_types=1);
namespace Dgarrido\CuantasMasApi\Transformers;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;

abstract class TransformerAbstract
{
    /**
     * Transforms a collection of Models.
     *
     * @param LengthAwarePaginator $items
     * @param string $resourceName
     * @return array
     */
    public function transformCollection(LengthAwarePaginator $items, string $resourceName): array
    {
        $url = config("cmas::settings.api_url") . "{$resourceName}/";
        $items->setPath($url);

        $result = [
            "items" => array_map([$this, 'transform'], $items->all()),
            "meta" => [
                "total" =>  $items->total(),
                "next_page" => $items->nextPageUrl($items->currentPage()),
                "previous_page" => $items->previousPageUrl(),
                "first_page" => $items->url(1),
                "last_page" => $items->url($items->lastPage()),
            ]
        ];

        return $result;
    }

    /**
     * Transform a single Model.
     *
     * @param Model $item
     * @return array
     */
    public abstract function transform(Model $item): array;
}
