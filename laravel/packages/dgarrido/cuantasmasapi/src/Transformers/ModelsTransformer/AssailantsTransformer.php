<?php declare(strict_types=1);
namespace Dgarrido\CuantasMasApi\Transformers\ModelsTransformer;

use Dgarrido\CuantasMasApi\Transformers\Hateoas;
use Dgarrido\CuantasMasApi\Transformers\TransformerAbstract;
use Illuminate\Database\Eloquent\Model;

class AssailantsTransformer extends TransformerAbstract
{
    use Hateoas;

    /**
     * Implements transform function for a single assailant.
     *
     * @param Model $assailant
     * @return array
     */
    public function transform(Model $assailant): array
    {
        $formattedAssailant = [
            'atacante_id' => $assailant['id'],
            'nombre' => $assailant['name'],
            'apellido' => $assailant['lastname'],
            'edad' => $assailant['age'],
            'temperancia' => $assailant['temperance'],
            'atento_de_suicidio' => (bool)$assailant['suicide_attempt'],
            'situacion_actual' => $assailant['current_situation'],
            'relacion_con_victima' => $assailant['relation_to_victim'],
            'feminicidio_id' => $assailant['femicideId'],
            ];
        $hateoas = $this->getLinks(config("cmas::settings.resources_display.assailants"), $assailant['id']);
        $result = array_merge($formattedAssailant, $hateoas);
        
        return $result;
    }
}