<?php declare(strict_types=1);
namespace Dgarrido\CuantasMasApi\Transformers\ModelsTransformer;

use Dgarrido\CuantasMasApi\Transformers\Hateoas;
use Dgarrido\CuantasMasApi\Transformers\TransformerAbstract;
use Illuminate\Database\Eloquent\Model;

class VictimsTransformer extends TransformerAbstract
{
    use Hateoas;

    /**
     * Implements transform function for a single victim.
     *
     * @param Model $victim
     * @return array
     */
    public function transform(Model $victim): array
    {
        $formattedVictim = [
            'victima_id' => $victim['id'],
            'nombre' => $victim['name'],
            'apellido' => $victim['lastname'],
            'edad' => $victim['age'],
            'agresiones_previas' => $victim['previous_aggresions'],
            'numero_de_hijos' => $victim['children'],
            'causa_de_muerte' => $victim['cause_of_death'],
            'feminicidio_id' => $victim['femicideId'],
            ];
            
        $hateoas = $this->getLinks(config("cmas::settings.resources_display.victims"), $victim['id']);
        $result = array_merge($formattedVictim, $hateoas);
        
        return $result;
    }
}