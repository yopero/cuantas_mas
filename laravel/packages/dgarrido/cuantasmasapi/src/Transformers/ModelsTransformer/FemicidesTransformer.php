<?php declare(strict_types=1);
namespace Dgarrido\CuantasMasApi\Transformers\ModelsTransformer;

use Dgarrido\CuantasMasApi\Transformers\Hateoas;
use Dgarrido\CuantasMasApi\Transformers\TransformerAbstract;
use Illuminate\Database\Eloquent\Model;

class FemicidesTransformer extends TransformerAbstract
{
    use Hateoas;

    /**
     * Femicides Transformer
     *
     * @var VictimsTransformer
     */
    protected $transformer;

    /**
     * FemicidesTransformer constructor
     *
     * @param VictimsTransformer $transformer
     * @param AssailantsTransformer $assailantTransformer
     * @param TsjCasesTransformer $tsjcaseTransformer
     * @param PressReleasesTransformer $pressReleasesTransformer
     * @param LocationsTransformer $locationsTranformer
     */
    public function __construct(
        VictimsTransformer $victimsTransformer, 
        AssailantsTransformer $assailantTransformer,
        TsjCasesTransformer $tsjcaseTransformer,
        PressReleasesTransformer $pressReleasesTransformer,
        LocationsTransformer $locationsTranformer
        )
    {
        $this->victimsTransformer = $victimsTransformer;
        $this->assailantTransformer = $assailantTransformer;
        $this->tsjcaseTransformer = $tsjcaseTransformer;
        $this->pressReleasesTransformer = $pressReleasesTransformer;
        $this->locationsTransformer = $locationsTranformer;

    }

    /**
     * transforms a Femicide with its relationships
     *          
     * @param  $femicide
     * @return array
     */
    public function transform(Model $femicide): array
    {
        $victim = $femicide->victim;
        $formattedVictim = $this->victimsTransformer->transform($victim);
        
        $assailant = $femicide->assailant;
        $formattedAssailant = $this->assailantTransformer->transform($assailant);
        
        $location = $femicide->location;
        $formattedLocation = $this->locationsTransformer->transform($location);
        
        $tsjCase = $femicide->tsjCase;
        $formattedTsjCase = $this->tsjcaseTransformer->transform($tsjCase);
        
        $pressReleases = $femicide->pressReleases();
        $formattedPressReleases = $this->pressReleasesTransformer->transformCollection($pressReleases, 'notas_de_prensa');
  
        $femicideData = $this->getFemicideData($femicide);

        $formattedFemicide = $femicideData + $this->addRelationships(
            $formattedVictim, 
            $formattedAssailant, 
            $formattedTsjCase, 
            $formattedPressReleases, 
            $formattedLocation
        );
       
        $hateoas = $this->getHateoas($femicide);

        $result = array_merge($formattedFemicide, $hateoas);

        return $result;
    }

    private function getFemicideData($femicide): array
    {
        return [
            'femicidio_id' => $femicide['id'],
            'fecha' => $femicide['date'],
            'circunstancias' => $femicide['circumstance'],
            'observaciones' => $femicide['notes'],
            'estado' => $femicide['status'],
        ];
    }

    private function addRelationships(
        $formattedVictim, 
        $formattedAssailant, 
        $formattedTsjCase, 
        $formattedPressReleases, 
        $formattedLocation
        ): array 
    {
        return [
            'victima' => $formattedVictim,
            'atacante' => $formattedAssailant,
            'lugar' => $formattedLocation,
            'tsj_caso' => $formattedTsjCase,
            'notas_de_prensa' => $formattedPressReleases
        ];
    }

    private function getHateoas($femicide): array
    {
        return $this->getLinks(config("cmas::settings.resources_display.femicides"), $femicide['id']);
    }

    public function getSimpleFormattedVictim($femicide)
    {
        return $this->getFemicideData($femicide) + $this->getHateoas($femicide);
    }
}