<?php declare(strict_types=1);
namespace Dgarrido\CuantasMasApi\Transformers\ModelsTransformer;

use Dgarrido\CuantasMasApi\Transformers\Hateoas;
use Dgarrido\CuantasMasApi\Transformers\TransformerAbstract;
use Illuminate\Database\Eloquent\Model;

class LocationsTransformer extends TransformerAbstract
{
    use Hateoas;


    /**
     * Implements transform function for a single location.
     *
     * @param Model $location
     * @return array
     */
    public function transform(Model $location): array
    {
        $formattedLocation = [
            'geolocalizacion_id' => $location['id'],
            'localidad' => $location['town'],
            'provincia' => $location['province'],
            'departamento' => $location['department'],
            'latitud' => $location['lat'],
            'longitud' => $location['lng'],
            'feminicidio_id' => $location['femicideId'],
            ];
        $hateoas = $this->getLinks(config("cmas::settings.resources_display.locations"), $location['id']);
        $result = array_merge($formattedLocation, $hateoas);
        
        return $result;
    }
}