<?php declare(strict_types=1);
namespace Dgarrido\CuantasMasApi\Transformers\ModelsTransformer;

use Dgarrido\CuantasMasApi\Transformers\Hateoas;
use Dgarrido\CuantasMasApi\Transformers\TransformerAbstract;
use Illuminate\Database\Eloquent\Model;

class TsjCasesTransformer extends TransformerAbstract
{
    use Hateoas;

    /**
     * Implements transform function for a single tsjCase.
     *
     * @param Model $tsjCase
     * @return array
     */
    public function transform(Model $tsj_case): array
    {
        $formattedTsjCase = [
            'tsj_id' => $tsj_case['id'],
            'sentencia' => $tsj_case['court_decision'],
            'fecha_sentencia' => $tsj_case['date_decision'],
            'tsj_link' => $tsj_case['link'],
            'feminicidio_id' => $tsj_case['femicideId'],
            ];
        $hateoas = $this->getLinks(config("cmas::settings.resources_display.tsj_cases"), $tsj_case['id']);
        $result = array_merge($formattedTsjCase, $hateoas);
        
        return $result;
    }
}