<?php declare(strict_types=1);
namespace Dgarrido\CuantasMasApi\Transformers\ModelsTransformer;

use Dgarrido\CuantasMasApi\Transformers\Hateoas;
use Dgarrido\CuantasMasApi\Transformers\TransformerAbstract;
use Illuminate\Database\Eloquent\Model;

class PressReleasesTransformer extends TransformerAbstract
{
    use Hateoas;


    /**
     * Implements transform function for a single pressRelease.
     *
     * @param Model $pressRelease
     * @return array
     */
    public function transform(Model $pressRelease): array
    {
        $formattePressRelease = [
            'nota_de_prensa_id' => $pressRelease['id'],
            'tipo' => $pressRelease['media_type'],
            'link' => $pressRelease['link'],
            'feminicidio_id' => $pressRelease['femicideId'],
            ];
        $hateoas = $this->getLinks(config("cmas::settings.resources_display.press_releases"), $pressRelease['id']);
        $result = array_merge($formattePressRelease, $hateoas);
        
        return $result;
    }
}