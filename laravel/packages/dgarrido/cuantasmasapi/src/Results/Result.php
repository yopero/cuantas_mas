<?php declare (strict_types=1);
namespace Dgarrido\CuantasMasApi\Results;

class Result
{
    /**
     * Code holding the status of the Result.
     *
     * @var string $code
     */
    protected $code;

    /**
     * Result's data/payload.
     *
     * @var array $data
     */
    protected $data;
    
    /**
     * Result's constructor.
     *
     * @param string $code
     * @param array $data
     */
    public function __construct(string $code, array $data = [])
    {
        $this->code = $code;
        $this->data = $data;
    }

    /**
     * Gets the current code of the Result.`
     *
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * Gets the data/payload of the Result.
     *
     * @return array
     */
    public function getData()
    {
        return $this->data[0];
    }
}