<?php declare(strict_types=1);
namespace Dgarrido\CuantasMasApi\Handlers\CommandHandlers;

use League\Csv\Reader;
use Dgarrido\CuantasMasApi\Parsers\CsvParser;
use Dgarrido\CuantasMasApi\Formatters\FemicideFormatter;
use Dgarrido\CuantasMasApi\Repository\FemicidesRepository;
use Dgarrido\CuantasMasApi\Models\Femicide;
use Dgarrido\CuantasMasApi\Models\Location;
use Dgarrido\CuantasMasApi\Models\Victim;
use Dgarrido\CuantasMasApi\Models\Assailant;
use Dgarrido\CuantasMasApi\Models\PressRelease;
use Dgarrido\CuantasMasApi\Models\TsjCase;

class CsvImporterCommandHandler
{
    protected $csvReader;

    protected $formatter;
    
    public function __construct(
        CsvParser $csvReader, 
        FemicideFormatter $formatter,
        Femicide $femicide,
        Location $location,
        Victim $victim,
        Assailant $assailant,
        PressRelease $pressReleases,
        TsjCase $tsjCase
        )
    {
        $this->csvReader = $csvReader;
        $this->formatter = $formatter;
        $this->repository = new FemicidesRepository($femicide, $location, $victim, $assailant, $pressReleases, $tsjCase);
    }

    public function saveRecords()
    {
        $records = $this->csvReader->getData();
        $header = $this->csvReader->getHeaders();

        foreach($records as $record){
            $femicideObject = $this->formatter->formatRow($record, $records->key());
            $this->repository->save($femicideObject);
        }
        
    }
    }