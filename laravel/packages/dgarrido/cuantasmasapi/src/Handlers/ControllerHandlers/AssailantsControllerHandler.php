<?php declare (strict_types=1);
namespace Dgarrido\CuantasMasApi\Handlers\ControllerHandlers;

use Dgarrido\CuantasMasApi\Repository\Repository;
use Dgarrido\CuantasMasApi\Models\Assailant;
use Dgarrido\CuantasMasApi\Transformers\ModelsTransformer\AssailantsTransformer;
use Dgarrido\CuantasMasApi\Results\Result;

class AssailantsControllerHandler
{
    /**
     * Model for the AssailantsController.
     *
     * @var Assailant
     */
    protected $model;
    
    /**
     * Transformer for the Assailant Model.
     *
     * @var AssailantsTransformer
     */
    protected $transformer;

    /**
     * AssailantsControllerHandler constructor.
     *
     * @param Assailant $assailant
     * @param AssailantsTransformer $transformer
     */
    public function __construct(Assailant $assailant, AssailantsTransformer $transformer)
    {
        $this->model = new Repository($assailant);
        $this->transformer = $transformer;
    }

    /**
     * Gets the Assailant from Database.``
     *
     * @param string $id
     * @return Result
     */
    public function getAssailant(string $id): Result
    {
        if(! is_numeric($id)){
            return new Result("not_number");
        }

        $result =  $this->model->show($id);
        if($result->getCode() == "not_found"){
            return $result;
        }
        
        $assailant = $result->getData();
        $formattedAssailant = $this->transformer->transform($assailant);

        return new Result("Ok", [$formattedAssailant]);
    }
    
    /**
     * Gets all the Assailants from Database.
     *
     * @return Result
     */
    public function getAssailantsCollection(): Result
    {
        $assailants = $this->model->index();
        $resourceName = config("cmas::settings.resources_display.assailants");
        $formattedAssailants = $this->transformer->transformCollection($assailants->getData(), $resourceName);
        if(! $formattedAssailants){
            return new Result("not_found");
        }

        return new Result("Ok", [$formattedAssailants]);
    }
}