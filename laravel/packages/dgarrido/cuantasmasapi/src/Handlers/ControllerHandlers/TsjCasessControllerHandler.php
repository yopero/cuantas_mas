<?php declare (strict_types=1);
namespace Dgarrido\CuantasMasApi\Handlers\ControllerHandlers;

use Dgarrido\CuantasMasApi\Repository\Repository;
use Dgarrido\CuantasMasApi\Models\TsjCase;
use Dgarrido\CuantasMasApi\Transformers\ModelsTransformer\TsjCasesTransformer;
use Dgarrido\CuantasMasApi\Results\Result;

class TsjCasesControllerHandler
{
    /**
     * Model for the TsjCasesController.
     *
     * @var TsjCase
     */
    protected $model;

    /**
     * Transformer for the TsjCase Model.
     *
     * @var TsjCasesTransformer
     */
    protected $transformer;

    /**
     * TsjCasesControllerHandler constructor.
     *
     * @param TsjCase $tsjCase
     * @param TsjCasesTransformer $transformer
     */
    public function __construct(TsjCase $tsjCase, TsjCasesTransformer $transformer)
    {
        $this->model = new Repository($tsjCase);
        $this->transformer = $transformer;
    }

    /**
     * Gets the TsjCase from Database.
     *
     * @return Result
     */
    public function getTsjCase(string $id): Result
    {
        if(! is_numeric($id)){
            return new Result("not_number");
        }

        $result =  $this->model->show($id);
        if($result->getCode() == "not_found"){
            return $result;
        }
        
        $tsjCase = $result->getData();
        $formattedTsjCase = $this->transformer->transform($tsjCase);

        return new Result("Ok", [$formattedTsjCase]);
    }

    /**
     * Gets all the TsjCases from Database.
     *
     * @return Result
     */
    public function getTsjCasesCollection(): Result
    {
        $tsjCases = $this->model->index();
        $resourceName = config("cmas::settings.resources_display.tsj_cases");
        $formattedTsjCases = $this->transformer->transformCollection($tsjCases->getData(), $resourceName);
        if(! $formattedTsjCases){
            return new Result("not_found");
        }

        return new Result("Ok", [$formattedTsjCases]);
    }
    
}