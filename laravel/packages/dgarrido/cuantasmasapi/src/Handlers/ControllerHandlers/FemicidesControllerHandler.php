<?php declare (strict_types=1);
namespace Dgarrido\CuantasMasApi\Handlers\ControllerHandlers;

use Dgarrido\CuantasMasApi\Repository\Repository;
use Dgarrido\CuantasMasApi\Models\Femicide;
use Dgarrido\CuantasMasApi\Transformers\ModelsTransformer\FemicidesTransformer;
use Dgarrido\CuantasMasApi\Results\Result;
use Illuminate\Pagination\LengthAwarePaginator;

class FemicidesControllerHandler
{
    /**
     * Model for the FemicidesController.
     *
     * @var Femicide
     */
    protected $model;

    /**
     * Transformer for the Femicide Model.
     *
     * @var FemicidesTransformer
     */
    protected $transformer;

    /**
     * FemicidesControllerHandler constructor.
     *
     * @param Femicide $femicide
     * @param FemicidesTransformer $transformer
     */
    public function __construct(
        Femicide $femicide,
        FemicidesTransformer $transformer
        )
    {
        $this->model = new Repository($femicide);
        $this->transformer = $transformer;
    }

    /**
     * Gets the Femicide from Database.
     *
     * @return Result
     */
    public function getFemicide(string $id): Result
    {
        if(! is_numeric($id)){
            return new Result("not_number");
        }

        $result =  $this->model->show($id);

        if($result->getCode() == "not_found"){
            return $result;
        }

        $femicide = $result->getData();
        $formattedFemicide = $this->transformer->transform($femicide);

        return new Result("Ok", [$formattedFemicide]);
    }

    /**
     * Gets all the Femicide from Database.
     *
     * @return Result
     */
    public function getFemicidesCollection(): Result
    {
        $femicides = $this->model->index();
        $resourceName = config("cmas::settings.resources_display.femicides");
        $formattedFemicides = $this->transformer->transformCollection($femicides->getData(), $resourceName);
        if(! $formattedFemicides){
            return new Result("not_found");
        }

        return new Result("Ok", [$formattedFemicides]);
    }

    /**
     * Gets all the Femicide from Database.
     *
     * @return Result
     */
    public function getFemicidesCollectionAll(): Result
    {
        $femicides = $this->model->indexAll();
        $resourceName = config("cmas::settings.resources_display.femicides");
        $formattedFemicides = $this->transformer->transformCollection($femicides->getData(), $resourceName);
        if(! $formattedFemicides){
            return new Result("not_found");
        }

        return new Result("Ok", [$formattedFemicides]);
    }

}
