<?php declare (strict_types=1);
namespace Dgarrido\CuantasMasApi\Handlers\ControllerHandlers;

use Dgarrido\CuantasMasApi\Repository\Repository;
use Dgarrido\CuantasMasApi\Models\Victim;
use Dgarrido\CuantasMasApi\Transformers\ModelsTransformer\VictimsTransformer;
use Dgarrido\CuantasMasApi\Results\Result;

class VictimsControllerHandler
{
    /**
     * Model for the VictimsController.
     *
     * @var Victim
     */
    protected $model;

    /**
     * Transformer for the Victim Model.
     *
     * @var VictimsTransformer
     */
    protected $transformer;
    /**
     * Gets the Victim from Database.
     *
     * @return Result
     */
    /**
     * VictimsControllerHandler constructor.
     *
     * @param Victim $victim
     * @param VictimsTransformer $transformer
     */
    public function __construct(Victim $victim, VictimsTransformer $transformer)
    {
        $this->model = new Repository($victim);
        $this->transformer = $transformer;
    }

    /**
     * Gets all the Victims from Database.
     *
     * @return Result
     */
    public function getVictim(string $id): Result
    {
        if(! is_numeric($id)){
            return new Result("not_number");
        }

        $result =  $this->model->show($id);
        if($result->getCode() == "not_found"){
            return $result;
        }
        
        $victim = $result->getData();
        $formattedVictim = $this->transformer->transform($victim);

        return new Result("Ok", [$formattedVictim]);
    }

    public function getVictimsCollection(): Result
    {
        $victims = $this->model->index();
        $resourceName = config("cmas::settings.resources_display.victims");
        $formattedVictims = $this->transformer->transformCollection($victims->getData(), $resourceName);
        if(! $formattedVictims){
            return new Result("not_found");
        }

        return new Result("Ok", [$formattedVictims]);
    }
    
}