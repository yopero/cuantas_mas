<?php declare (strict_types=1);
namespace Dgarrido\CuantasMasApi\Handlers\ControllerHandlers;

use Dgarrido\CuantasMasApi\Repository\Repository;
use Dgarrido\CuantasMasApi\Models\PressRelease;
use Dgarrido\CuantasMasApi\Transformers\ModelsTransformer\PressReleasesTransformer;
use Dgarrido\CuantasMasApi\Results\Result;

class PressReleasesControllerHandler
{
    /**
     * Model for the PressReleasesController.
     *
     * @var PressRelease
     */
    protected $model;

    /**
     * Transformer for the PressRelease Model.
     *
     * @var PressReleasesTransformer
     */
    protected $transformer;

    /**
     * PressReleasesControllerHandler constructor.
     *
     * @param PressRelease $pressRelease
     * @param PressReleasesTransformer $transformer
     */
    public function __construct(PressRelease $pressRelease, PressReleasesTransformer $transformer)
    {
        $this->model = new Repository($pressRelease);
        $this->transformer = $transformer;
    }

    /**
     * Gets the PressRelease from Database.
     *
     * @return Result
     */
    public function getPressRelease(string $id): Result
    {
        if(! is_numeric($id)){
            return new Result("not_number");
        }

        $result =  $this->model->show($id);
        if($result->getCode() == "not_found"){
            return $result;
        }
        
        $pressRelease = $result->getData();
        $formattedPressRelease = $this->transformer->transform($pressRelease);

        return new Result("Ok", [$formattedPressRelease]);
    }

    /**
     * Gets all the PressRelease from Database.
     *
     * @return Result
     */
    public function getPressReleasesCollection(): Result
    {
        $pressReleases = $this->model->index();
        $resourceName = config("cmas::settings.resources_display.press_releases");
        $formattedPressReleases = $this->transformer->transformCollection($pressReleases->getData(), $resourceName);
        if(! $formattedPressReleases){
            return new Result("not_found");
        }

        return new Result("Ok", [$formattedPressReleases]);
    }
    
}