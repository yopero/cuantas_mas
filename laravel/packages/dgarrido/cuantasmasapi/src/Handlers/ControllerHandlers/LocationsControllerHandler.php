<?php declare (strict_types=1);
namespace Dgarrido\CuantasMasApi\Handlers\ControllerHandlers;

use Dgarrido\CuantasMasApi\Repository\Repository;
use Dgarrido\CuantasMasApi\Models\Location;
use Dgarrido\CuantasMasApi\Transformers\ModelsTransformer\LocationsTransformer;
use Dgarrido\CuantasMasApi\Results\Result;

class LocationsControllerHandler
{
    /**
     * Model for the LocationsController.
     *
     * @var Location
     */
    protected $model;

    /**
     * Transformer for the Location Model.
     *
     * @var LocationsTransformer
     */
    protected $transformer;

    /**
     * LocationsControllerHandler constructor.
     *
     * @param Location $location
     * @param LocationsTransformer $transformer
     */
    public function __construct(Location $location, LocationsTransformer $transformer)
    {
        $this->model = new Repository($location);
        $this->transformer = $transformer;
    }

    /**
     * Gets the Location from Database.
     *
     * @return Result
     */
    public function getLocation(string $id): Result
    {
        if(! is_numeric($id)){
            return new Result("not_number");
        }

        $result =  $this->model->show($id);
        if($result->getCode() == "not_found"){
            return $result;
        }
        
        $location = $result->getData();
        $formattedLocation = $this->transformer->transform($location);

        return new Result("Ok", [$formattedLocation]);
    }

    /**
     * Gets all the Locations from Database.
     *
     * @return Result
     */
    public function getLocationsCollection(): Result
    {
        $locations = $this->model->index();
        $resourceName = config("cmas::settings.resources_display.locations");
        $formattedLocations = $this->transformer->transformCollection($locations->getData(), $resourceName);
        if(! $formattedLocations){
            return new Result("not_found");
        }

        return new Result("Ok", [$formattedLocations]);
    }
    
}