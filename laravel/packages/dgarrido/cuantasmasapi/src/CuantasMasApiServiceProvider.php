<?php declare (strict_types=1);

namespace Dgarrido\CuantasMasApi;

use Illuminate\Support\ServiceProvider;
use Dgarrido\CuantasMasApi\Commands\CsvImporter\CsvImporter;

class CuantasMasApiServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->loadRoutesFrom(__DIR__ . '/Routes/web.php');
        $this->loadMigrationsFrom(realpath(__DIR__ . '/../database/migrations/'));
        $this->publishes([
            __DIR__ . '/../database/factories/' => database_path('factories')
        ], 'factories');
        $this->publishes([
            __DIR__ . '/../database/seeds' => base_path('database/seeds'),
        ]);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->registerCommands();
        $this->mergeConfigFrom(
            __DIR__ . '/../config/settings.php',
            'cmas::settings'
        );
       
    }

    /**
     * Registers commands to Laravel' app. 
     *
     * @return void
     */
    private function registerCommands(): void
    {
        $this->commands(CsvImporter::class);
    }

}
