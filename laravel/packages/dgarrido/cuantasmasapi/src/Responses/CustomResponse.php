<?php declare(strict_types=1);
namespace Dgarrido\CuantasMasApi\Responses;

use Illuminate\Http\JsonResponse;
use Dgarrido\CuantasMasApi\Results\Result;

class CustomResponse
{
    /**
     * Make Response.
     *
     * @param Result $data
     * @return JsonResponse
     */
    public function make(Result $data): JsonResponse
    {
        if($data->getCode() == "not_number"){
            return $this->respondNotDigit();
        }
        if($data->getCode() == "not_found"){
            return $this->respondNotFound();
        }
        if($data->getCode() == "Ok"){
            return $this->successResponse($data->getData());
        }
    }
    
    /**
     * Succesful Response.
     *
     * @param array $data
     * @return JsonResponse
     */
    private function successResponse(array $data): JsonResponse
    {
        return response()->json([
            'data' => $data
        ]);
    }

    /**
     * Not found Response.
     *
     * @param string $message
     * @return JsonResponse
     */
    private function respondNotFound(string $message = "Not Found"): JsonResponse
    {
        $httpCode = 404;
        return response()->json([
            'error' => [
                'message' => $message,
                'status_code' => $httpCode
            ]
            ], $httpCode);
    }

    /**
     * Not valid id Response.
     *
     * @param string $message
     * @return JsonResponse
     */
    private function respondNotDigit(string $message = "Id must be a number"): JsonResponse
    {
        $statusCode = 400;
        return response()->json([
            'error' => [
                'message' => $message,
                'status_code' => $statusCode
            ]
            ], $statusCode);
    }
}