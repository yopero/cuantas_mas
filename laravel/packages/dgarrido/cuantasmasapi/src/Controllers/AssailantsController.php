<?php declare(strict_types=1);
namespace Dgarrido\CuantasMasApi\Controllers;

use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Dgarrido\CuantasMasApi\Responses\CustomResponse;
use Dgarrido\CuantasMasApi\Handlers\ControllerHandlers\AssailantsControllerHandler;

class AssailantsController extends Controller
{
    /**
     * Handler for the Assailants Controller.
     *
     * @var AssailantsControllerHandler
     */
    protected $handler;
    
    /**
     * Custom Response.
     *
     * @var CustomResponse
     */
    protected $response;

    /**
     * Assailants Controller's constructor.
     *
     * @param AssailantsControllerHandler $handler
     * @param CustomResponse $response
     */
    public function __construct(AssailantsControllerHandler $handler, CustomResponse $response)
    {
        $this->handler = $handler;
        $this->response = $response;
    }

    /**
     * Returns a collection of all assailants.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
       $formattedCollection  =$this->handler->getAssailantsCollection();
       
       return $this->response->make($formattedCollection);
    }
    
    /**
     * Returns an Assailant.
     *
     * @param string $assailant
     * @return JsonResponse
     */
    public function show(string $assailant): JsonResponse
    {
        $result = $this->handler->getAssailant($assailant);
        
        return $this->response->make($result);
    }
}
