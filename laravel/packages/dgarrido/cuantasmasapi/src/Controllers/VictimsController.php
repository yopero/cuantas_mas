<?php declare(strict_types=1);
namespace Dgarrido\CuantasMasApi\Controllers;

use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Dgarrido\CuantasMasApi\Responses\CustomResponse;
use Dgarrido\CuantasMasApi\Handlers\ControllerHandlers\VictimsControllerHandler;

class VictimsController extends Controller
{
    /**
     * Victims Controller.
     *
     * @var VictimsControllerHandler
     */
    protected $handler;

    /**
     * Response.
     *
     * @var VictimsControllerHandler  
     */
    protected $response;

    /**
     * Victims Controller constructor.
     *
     * @param VictimsControllerHandler $handler
     * @param CustomResponse $response
     */
    public function __construct(VictimsControllerHandler $handler, CustomResponse $response)
    {
        $this->handler = $handler;
        $this->response = $response;
    }

    /**
     * Returns a Victims collection Response.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
       $formattedCollection  =$this->handler->getVictimsCollection();
       return $this->response->make($formattedCollection);
    }

    /**
     * Returns a Victim Response.
     *
     * @return JsonResponse
     */
    public function show(string $victim): JsonResponse
    {
        $result = $this->handler->getVictim($victim);
        
        return $this->response->make($result);
    }

}
