<?php declare(strict_types=1);
namespace Dgarrido\CuantasMasApi\Controllers;

use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Dgarrido\CuantasMasApi\Responses\CustomResponse;
use Dgarrido\CuantasMasApi\Handlers\ControllerHandlers\FemicidesControllerHandler;

class FemicidesController extends Controller
{
    /**
     * Femicides Controller.
     *
     * @var FemicidesControllerHandler
     */
    protected $handler;

    /**
     * Response.
     *
     * @var FemicidesControllerHandler
     */
    protected $response;

    /**
     * Femicides Controller constructor.
     *
     * @param FemicidesControllerHandler $handler
     * @param CustomResponse $response
     */
    public function __construct(FemicidesControllerHandler $handler, CustomResponse $response)
    {
        $this->handler = $handler;
        $this->response = $response;
    }

    /**
     * Returns a Victims collection Response.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
       $formattedCollection  =$this->handler->getFemicidesCollection();
       return $this->response->make($formattedCollection);
    }

    /**
     * Returns a Victims collection Response.
     *
     * @return JsonResponse
     */
    public function indexAll(): JsonResponse
    {
       $formattedCollection  =$this->handler->getFemicidesCollectionAll();
       return $this->response->make($formattedCollection);
    }

    /**
     * Returns a Victim Response.
     *
     * @param string $femicide
     * @return JsonResponse
     */
    public function show(string $femicide): JsonResponse
    {
        $result = $this->handler->getFemicide($femicide);

        return $this->response->make($result);
    }

}
