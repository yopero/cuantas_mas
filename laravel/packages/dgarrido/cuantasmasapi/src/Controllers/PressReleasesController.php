<?php declare(strict_types=1);
namespace Dgarrido\CuantasMasApi\Controllers;

use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Dgarrido\CuantasMasApi\Responses\CustomResponse;
use Dgarrido\CuantasMasApi\Handlers\ControllerHandlers\PressReleasesControllerHandler;

class PressReleasesController extends Controller
{
    /**
     * PressReleases Controller.
     *
     * @var PressReleasesControllerHandler
     */
    protected $handler;

    /**
     * Response.
     *
     * @var PressReleasesControllerHandler  
     */
    protected $response;

    /**
     * PressReleases Controller constructor.
     *
     * @param PressReleasesControllerHandler $handler
     * @param CustomResponse $response
     */
    public function __construct(PressReleasesControllerHandler $handler, CustomResponse $response)
    {
        $this->handler = $handler;
        $this->response = $response;
    }

    /**
     * Returns a PressReleases collection Response.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
       $formattedCollection  =$this->handler->getPressReleasesCollection();
       return $this->response->make($formattedCollection);
    }

    /**
     * Returns a PressRelease Response.
     *
     * @return JsonResponse
     */
    public function show(string $pressRelease): JsonResponse
    {
        $result = $this->handler->getPressRelease($pressRelease);
        
        return $this->response->make($result);
    }

}
