<?php declare(strict_types=1);
namespace Dgarrido\CuantasMasApi\Controllers;

use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Dgarrido\CuantasMasApi\Responses\CustomResponse;
use Dgarrido\CuantasMasApi\Handlers\ControllerHandlers\LocationsControllerHandler;

class LocationsController extends Controller
{
    /**
     * Locations Controller.
     *
     * @var LocationsControllerHandler
     */
    protected $handler;

    /**
     * Response.
     *
     * @var LocationsControllerHandler  
     */
    protected $response;

    /**
     * Locations Controller constructor.
     *
     * @param LocationsControllerHandler $handler
     * @param CustomResponse $response
     */
    public function __construct(LocationsControllerHandler $handler, CustomResponse $response)
    {
        $this->handler = $handler;
        $this->response = $response;
    }

    /**
     * Returns a Locations collection Response.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
       $formattedCollection  =$this->handler->getLocationsCollection();
       return $this->response->make($formattedCollection);
    }

    /**
     * Returns a Location Response.
     *
     * @return JsonResponse
     */
    public function show(string $location): JsonResponse
    {
        $result = $this->handler->getLocation($location);
        
        return $this->response->make($result);
    }

}
