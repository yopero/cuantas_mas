<?php declare(strict_types=1);
namespace Dgarrido\CuantasMasApi\Controllers;

use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Dgarrido\CuantasMasApi\Responses\CustomResponse;
use Dgarrido\CuantasMasApi\Handlers\ControllerHandlers\TsjCasesControllerHandler;

class TsjCasesController extends Controller
{
    /**
     * TsjCases Controller.
     *
     * @var TsjCasesControllerHandler
     */
    protected $handler;

    /**
     * Response.
     *
     * @var TsjCasesControllerHandler  
     */
    protected $response;

    /**
     * TsjCases Controller constructor.
     *
     * @param TsjCasesControllerHandler $handler
     * @param CustomResponse $response
     */
    public function __construct(TsjCasesControllerHandler $handler, CustomResponse $response)
    {
        $this->handler = $handler;
        $this->response = $response;
    }

    /**
     * Returns a TsjCases collection Response.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
       $formattedCollection  =$this->handler->getTsjCasesCollection();
       return $this->response->make($formattedCollection);
    }

    /**
     * Returns a TsjCase Response.
     *
     * @return JsonResponse
     */
    public function show(string $tsj_case): JsonResponse
    {
        $result = $this->handler->getTsjCase($tsj_case);
        
        return $this->response->make($result);
    }

}
