<?php declare(strict_types=1);


Route::prefix('api')->group(function () {
    $victims_route_display = config("cmas::settings.resources_display.victims");
    $femicides_route_display = config("cmas::settings.resources_display.femicides");
    $assailants_route_display = config("cmas::settings.resources_display.assailants");
    $locations_route_display = config("cmas::settings.resources_display.locations");
    $tsj_cases_route_display = config("cmas::settings.resources_display.tsj_cases");
    $press_releases_route_display = config("cmas::settings.resources_display.press_releases");

    Route::get(
        $victims_route_display . "/{id}",
        'Dgarrido\CuantasMasApi\Controllers\VictimsController@show'
    );

    Route::get(
        $victims_route_display,
        'Dgarrido\CuantasMasApi\Controllers\VictimsController@index'
    );

    Route::get(
        $assailants_route_display . "/{id}",
        'Dgarrido\CuantasMasApi\Controllers\AssailantsController@show'
    );

    Route::get(
        $assailants_route_display,
        'Dgarrido\CuantasMasApi\Controllers\AssailantsController@index'
    );

    Route::get(
        $locations_route_display . "/{id}",
        'Dgarrido\CuantasMasApi\Controllers\LocationsController@show'
    );

    Route::get(
        $locations_route_display,
        'Dgarrido\CuantasMasApi\Controllers\LocationsController@index'
    );

    Route::get(
        $tsj_cases_route_display. "/{id}",
        'Dgarrido\CuantasMasApi\Controllers\TsjCasesController@show'
    );

    Route::get(
        $tsj_cases_route_display,
        'Dgarrido\CuantasMasApi\Controllers\TsjCasesController@index'
    );

    Route::get(
        $press_releases_route_display . "/{id}",
        'Dgarrido\CuantasMasApi\Controllers\PressReleasesController@show'
    );

    Route::get(
        $press_releases_route_display,
        'Dgarrido\CuantasMasApi\Controllers\PressReleasesController@index'
    );

    Route::get(
      $femicides_route_display . "/all",
      'Dgarrido\CuantasMasApi\Controllers\FemicidesController@indexAll'
    );

    Route::get(
        $femicides_route_display . "/{id}",
        'Dgarrido\CuantasMasApi\Controllers\FemicidesController@show'
    );

    Route::get(
        $femicides_route_display,
        'Dgarrido\CuantasMasApi\Controllers\FemicidesController@index'
    );
});
