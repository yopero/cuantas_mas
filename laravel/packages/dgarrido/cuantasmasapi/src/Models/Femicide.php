<?php declare (strict_types=1);
namespace Dgarrido\CuantasMasApi\Models;

use Illuminate\Database\Eloquent\Model;
use Dgarrido\CuantasMasApi\Models\Victim;
use Dgarrido\CuantasMasApi\Transformers\ModelsTransformer\VictimsTransformer;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Pagination\LengthAwarePaginator;

class Femicide extends Model
{

    protected $fillable = ['date', 'circumstance', 'notes', 'status'];
    public $timestamps = true;
    /**
     * Returns the victim associated to the femicide.
     *
     * @return HasOne
     */
    public function victim(): HasOne
    {
        return $this->hasOne('Dgarrido\CuantasMasApi\Models\Victim', 'femicideId');
    }

    /**
     * Returns the assailant associated to the femicide.
     *
     * @return HasOne
     */
    public function assailant(): HasOne
    {
        return $this->hasOne('Dgarrido\CuantasMasApi\Models\Assailant', 'femicideId');
    }

    /**
     * Returns the TsjCase associated to the femicide.
     *
     * @return HasOne
     */
    public function tsjCase(): HasOne
    {
        return $this->hasOne('Dgarrido\CuantasMasApi\Models\TsjCase', 'femicideId');
    }

    /**
     * Returns the Location associated to the femicide.
     *
     * @return HasOne
     */
    public function location(): HasOne
    {
        return $this->hasOne('Dgarrido\CuantasMasApi\Models\Location', 'femicideId');
    }

    /**
     * Returns the Press Releases associated to the femicide.
     *
     * @return HasOne
     */
    public function pressReleases(): LengthAwarePaginator
    {
        return $this->hasMany('Dgarrido\CuantasMasApi\Models\PressRelease', 'femicideId')->paginate();
    }
}