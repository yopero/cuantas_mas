<?php declare (strict_types=1);
namespace Dgarrido\CuantasMasApi\Models;

use Illuminate\Database\Eloquent\Model;

class PressRelease extends Model
{
    protected $fillable = ['id', 'media_type', 'link', 'femicideId'];
}
