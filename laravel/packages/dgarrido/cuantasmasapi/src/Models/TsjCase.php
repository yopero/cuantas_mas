<?php declare (strict_types=1);
namespace Dgarrido\CuantasMasApi\Models;

use Illuminate\Database\Eloquent\Model;

class TsjCase extends Model
{
    protected $fillable = [
        'femicideId',
        'court_decision',
        'date_decision',
        'link',
    ];
}
