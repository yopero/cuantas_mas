<?php declare (strict_types=1);
namespace Dgarrido\CuantasMasApi\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = ['town', 'province', 'department', 'lat', 'lng', 'femicideId'];
}
