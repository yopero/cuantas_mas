<?php declare (strict_types=1);
namespace Dgarrido\CuantasMasApi\Models;

use Illuminate\Database\Eloquent\Model;

class Assailant extends Model
{
    protected $fillable = [
        'femicideId', 
        'name', 
        'lastname', 
        'age',
        'temperance',
        'suicide_attempt',
        'current_situation',
        'relation_to_victim' 
        ];
}
