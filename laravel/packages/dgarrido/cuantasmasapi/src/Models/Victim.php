<?php declare (strict_types=1);
namespace Dgarrido\CuantasMasApi\Models;

use Illuminate\Database\Eloquent\Model;

class Victim extends Model
{
    protected $fillable = ['femicideId', 'name', 'lastname', 'age', 'previous_aggresions', 'children', 'cause_of_death'];
}
