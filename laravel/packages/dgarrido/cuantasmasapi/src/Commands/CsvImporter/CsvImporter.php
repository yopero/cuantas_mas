<?php declare(strict_types=1);
namespace Dgarrido\CuantasMasApi\Commands\CsvImporter;

use Illuminate\Console\Command;
use Dgarrido\CuantasMasApi\Handlers\CommandHandlers\CsvImporterCommandHandler;

class CsvImporter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cuantasmas:import-csv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports data from csv file to DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Handle Command
     *
     * @param CsvImporterCommandHandler $handler
     * @return void
     */
    public function handle(CsvImporterCommandHandler $handler): void
    {
        $this->info("Importing data from csv file into the Database...");
        
        $time = -microtime(true);
        $handler->saveRecords();
        $time += microtime(true);
        
        $this->info("Processing data took: {$time} seconds.");

    }
}
