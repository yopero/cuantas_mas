<?php declare(strict_types=1);
namespace Dgarrido\CuantasMasApi\Parsers;

use League\Csv\Reader;
use League\Csv\MapIterator;

class CsvParser
{   
    protected $csvFile;

    public function __construct()
    {
        $this->csvFile = __DIR__ . "/db.csv";
    }
    
    public function getData(): MapIterator
    {
        $reader = Reader::createFromPath($this->csvFile, 'r');
        $reader->setHeaderOffset(0);
        
        return $reader->getRecords();
    }
    
    public function getHeaders(): array
    {
        $reader = Reader::createFromPath($this->csvFile, 'r');
        $reader->setHeaderOffset(0);
        return $reader->getHeader();
    }
}