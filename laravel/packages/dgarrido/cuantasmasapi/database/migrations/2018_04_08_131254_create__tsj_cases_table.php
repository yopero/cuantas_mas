<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTsjCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tsj_cases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('femicideId');
            $table->text('court_decision')->nullable();
            $table->date('date_decision')->nullable();
            $table->string('link', 2083)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_tsj_cases');
    }
}
