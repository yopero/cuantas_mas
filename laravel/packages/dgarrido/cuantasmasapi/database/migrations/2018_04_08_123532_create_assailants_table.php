<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssailantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assailants', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('femicideId');
            $table->string('name')->nullable();
            $table->string('lastname')->nullable();
            $table->integer('age')->nullable();
            $table->string('temperance')->nullable();
            $table->boolean('suicide_attempt')->nullable();
            $table->string('current_situation')->nullable();
            $table->string('relation_to_victim')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('authors');
    }
}
