<?php

use Faker\Generator as Faker;

$factory->define(Dgarrido\CuantasMasApi\Models\Femicide::class, function (Faker $faker) {
    return [
        'id' => $faker->unique()->numberBetween($min = 1, $max = 50),
        'date' => $faker->date,
        'circumstance' => $faker->paragraph(rand(2,5), true),
        'notes' => $faker->sentence(),
        'status' => $faker->word
    ];
});
