<?php
namespace Dgarrido\CuantasMasApi\PhpUnit\Result;

use Dgarrido\CuantasMasApi\PhpUnit\TestCase;
use Dgarrido\CuantasMasApi\Results\Result;

class ResultTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_get_code()
    {
        $result = new Result("Ok", ["my_data"]);
        $this->assertEquals($result->getCode(), "Ok");
    }

    public function test_get_not_get_code()
    {
        $result = new Result("Ok", ["my_data"]);
        $this->assertNotEquals($result->getCode(), "notok");
    }
    
    public function test_can_get_data()
    {
        $result = new Result("Ok", ["my_data"]);
        $this->assertEquals($result->getData(), "my_data");
    }
    
    public function test_result_data_is_array()
    {
        $arrayToAdd = ["my_data"];
        $result = new Result("Ok", [$arrayToAdd]);
        $this->assertInternalType('array', $result->getData());
    }
}
