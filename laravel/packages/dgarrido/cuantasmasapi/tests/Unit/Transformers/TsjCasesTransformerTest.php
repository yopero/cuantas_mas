<?php
namespace Dgarrido\CuantasMasApi\PhpUnit\Transformer;

use Dgarrido\CuantasMasApi\PhpUnit\TestCase;
use Dgarrido\CuantasMasApi\Transformers\ModelsTransformer\TsjCasesTransformer;
use Mockery as m;

class TsjCasesTransformerTest extends TestCase
{
    /**
     * Test tsj_cases Transformer.
     *
     * @return void
     */
    public function test_tsj_cases_transformer()
    {
        $tsj_case = $this->getTsjCase();
        $transformer = new TsjCasesTransformer();
        $transformer = m::mock(TsjCasesTransformer::class)->makePartial();
        $transformer->shouldReceive('getLinks')
            ->with('tsj_cases', $tsj_case->id)
            ->andReturn($this->hateoas('tsj_cases', $tsj_case->id));
        $expected = [
            'tsj_id' => 47,
            'sentencia' => "Voluptatibus voluptatem sit non ipsa. Nemo sunt in tempora sint error. Officiis ducimus laudantium minus sunt incidunt et.",
            'fecha_sentencia' => "1999-03-12",
            'tsj_link' => "http://hermann.com/aperiam-nam-quos-nam-sit-ad-nostrum-natus-temporibus",
            "feminicidio_id" => 5,
            "links" => ["self" => "http://localhost:9009/api/tsj_cases/47",
                        "index" => "http://localhost:9009/api/tsj_cases/"]
        ];

        $this->assertEquals($expected, $transformer->transform($tsj_case));
    }
}