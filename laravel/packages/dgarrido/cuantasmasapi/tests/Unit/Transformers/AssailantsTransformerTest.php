<?php
namespace Dgarrido\CuantasMasApi\PhpUnit\Transformer;

use Dgarrido\CuantasMasApi\PhpUnit\TestCase;
use Dgarrido\CuantasMasApi\Transformers\ModelsTransformer\AssailantsTransformer;
use Mockery as m;

class AssailantsTransformerTest extends TestCase
{
    /**
     * Test Assailants Transformer.
     *
     * @return void
     */
    public function test_assailants_transformer()
    {
        $assailant = $this->getAssailant();
        $transformer = new AssailantsTransformer();
        $transformer = m::mock(AssailantsTransformer::class)->makePartial();
        $transformer->shouldReceive('getLinks')
            ->with('assailants', $assailant->id)
            ->andReturn($this->hateoas('assailants', $assailant->id));
      
        $expected = [
                "atacante_id" => 1,
                "nombre" => "Enrique",
                "apellido" => "Bartoletti",
                "edad" => 35,
                "temperancia" => "ut sapiente",
                "atento_de_suicidio" => false,
                "situacion_actual" => "debitis dolores doloremque",
                "relacion_con_victima" => "magnam saepe",
                'feminicidio_id' => 5,
                "links" => ["self" => "http://localhost:9009/api/assailants/1",
                            "index" => "http://localhost:9009/api/assailants/"]
        ];
        $this->assertEquals($expected, $transformer->transform($assailant));
    }
}