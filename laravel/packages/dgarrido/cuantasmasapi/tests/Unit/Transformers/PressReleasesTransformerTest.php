<?php
namespace Dgarrido\CuantasMasApi\PhpUnit\Transformer;

use Dgarrido\CuantasMasApi\PhpUnit\TestCase;
use Dgarrido\CuantasMasApi\Transformers\ModelsTransformer\PressReleasesTransformer;
use Mockery as m;

class PressReleasesTransformerTest extends TestCase
{
    /**
     * Test PressReleases Transformer.
     *
     * @return void
     */
    public function test_press_releases_transformer()
    {
        $pressRelease = $this->getPressRelease();
        $transformer = new PressReleasesTransformer();
        $transformer = m::mock(PressReleasesTransformer::class)->makePartial();
        $transformer->shouldReceive('getLinks')
            ->with('press_releases', $pressRelease->id)
            ->andReturn($this->hateoas('press_releases', $pressRelease->id));
        $expected = [
            "nota_de_prensa_id" => 1,
            "tipo" => "commodi",
            "link" => "http://rowe.com/",
            "feminicidio_id" => 5,
            "links" => ["self" => "http://localhost:9009/api/press_releases/1",
                        "index" => "http://localhost:9009/api/press_releases/"]
        ];

        $this->assertEquals($expected, $transformer->transform($pressRelease));
    }
}