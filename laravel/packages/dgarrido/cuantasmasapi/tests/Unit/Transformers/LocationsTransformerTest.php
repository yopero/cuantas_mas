<?php

namespace Dgarrido\CuantasMasApi\PhpUnit\Transformer;

use Dgarrido\CuantasMasApi\PhpUnit\TestCase;
use Dgarrido\CuantasMasApi\Transformers\ModelsTransformer\LocationsTransformer;

use Mockery as m;

class LocationsTransformerTest extends TestCase
{
    /**
     * Test locations Transformer.
     *
     * @return void
     */
    public function test_location_transformer()
    {
        $location = $this->getLocation();
        $transformer = new LocationsTransformer();
        $transformer = m::mock(LocationsTransformer::class)->makePartial();
        $transformer->shouldReceive('getLinks')
            ->with('locations', $location->id)
            ->andReturn($this->hateoas('locations', $location->id));
        $expected = [
            'geolocalizacion_id' => 47,
            'localidad' => "Port Nakiaberg",
            'provincia' => "Bessiemouth",
            'departamento' => "Hawaii",
            'latitud' => "4.1570760",
            'longitud' => "126.6622380",
            "feminicidio_id" => 5,
            "links" => ["self" => "http://localhost:9009/api/locations/47",
                        "index" => "http://localhost:9009/api/locations/"]
        ];

        $this->assertEquals($expected, $transformer->transform($location));
    }
}