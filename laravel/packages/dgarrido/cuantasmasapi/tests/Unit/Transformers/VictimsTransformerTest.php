<?php
namespace Dgarrido\CuantasMasApi\PhpUnit\Transformer;

use Dgarrido\CuantasMasApi\PhpUnit\TestCase;
use Dgarrido\CuantasMasApi\Transformers\ModelsTransformer\VictimsTransformer;
use Mockery as m;

class VictimsTransformerTest extends TestCase
{
    /**
     * Test Victims Transformer.
     *
     * @return void
     */
    public function test_victims_transformer()
    {
        $victim = $this->getVictim();
        $transformer = new VictimsTransformer();
        $transformer = m::mock(VictimsTransformer::class)->makePartial();
        $transformer->shouldReceive('getLinks')
            ->with('victims', $victim->id)
            ->andReturn($this->hateoas('victims', $victim->id));
        $expected = [
            "victima_id" => 1,
            "nombre" => "Adrienne",
            "apellido" => "Bartoletti",
            "edad" => 35,
            "agresiones_previas" => "ut sapiente",
            "numero_de_hijos" => 9,
            "cause_de_muerte" => "debitis dolores doloremque",
            "feminicidio_id" => 5,
            "links" => ["self" => "http://localhost:9009/api/victims/1",
                        "index" => "http://localhost:9009/api/victims/"]
        ];

        $this->assertEquals($expected, $transformer->transform($victim));
    }
}