<?php

namespace Dgarrido\CuantasMasApi\PhpUnit\Transformer;

use Dgarrido\CuantasMasApi\PhpUnit\TestCase;
use Dgarrido\CuantasMasApi\Models\Femicide;
use Dgarrido\CuantasMasApi\Transformers\ModelsTransformer\FemicidesTransformer;
use Mockery as m;

class FemicidesTransformerTest extends TestCase
{
    /**
     * Test Femicides Transformer.
     *
     * @return void
     */
    public function test_femicides_transformer()
    {
        $attributes = [
            "id" => 1,
            "date" => "2001-02-19",
            "circumstance" => "Enim sunt voluptatem quas. Explicabo et aut facilis officia. Cum aut qui quo velit fugit quae. Molestiae qui deserunt rem.",
            "notes" => "Qui et eaque est.",
            "status" => "Bartoletti",
            "created_at" => "2018-04-14 21:20:39",
            "updated_at" => "2018-04-14 21:20:39",
        ];
        $femicide = new Femicide();
        $femicide->setRawAttributes($attributes);
        $transformer = m::mock(FemicidesTransformer::class)->makePartial();
        $transformer->shouldReceive('getLinks')
            ->with('femicides', $femicide->id)
                ->andReturn($this->hateoas('femicides', $femicide->id));
        
        $expected = [
            "femicidio_id" => 1,
            "fecha" => "2001-02-19",
            "circunstancias" => "Enim sunt voluptatem quas. Explicabo et aut facilis officia. Cum aut qui quo velit fugit quae. Molestiae qui deserunt rem.",
            "observaciones" => "Qui et eaque est.",
            "estado" => "Bartoletti",
            "links" => ["self" => "http://localhost:9009/api/femicides/1",
                        "index" => "http://localhost:9009/api/femicides/"]
        ];

        $this->assertEquals($expected, $transformer->getSimpleFormattedVictim($femicide));
    }
}