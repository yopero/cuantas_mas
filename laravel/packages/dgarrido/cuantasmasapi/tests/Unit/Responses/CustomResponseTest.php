<?php
namespace Dgarrido\CuantasMasApi\PhpUnit\Responses;

use Dgarrido\CuantasMasApi\PhpUnit\TestCase;
use Dgarrido\CuantasMasApi\Responses\CustomResponse;
use Dgarrido\CuantasMasApi\Results\Result;

class CustomResponseTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_response_200()
    {
        $this->markTestSkipped('PHPUnit will skip this test that will run only when app is running.');
        $response = new CustomResponse();
        $result = new Result("Ok", ["my_data"]);
        
        $responseCreated = $response->make($result);
        $this->assertEquals(200, $responseCreated->status());
    }
    
    public function test_response_404()
    {
        $this->markTestSkipped('PHPUnit will skip this test that will run only when app is running.');
        $response = new CustomResponse();
        $result = new Result("not_found", ["my_data"]);
        
        $responseCreated = $response->make($result);
        $this->assertEquals(404, $responseCreated->status());
    }
    
    public function test_response_400()
    {
        $this->markTestSkipped('PHPUnit will skip this test that will run only when app is running.');
        $response = new CustomResponse();
        $result = new Result("not_number", ["my_data"]);
        
        $responseCreated = $response->make($result);
        $this->assertEquals(400, $responseCreated->status());
    }

    public function test_response_200_content()
    {
        $this->markTestSkipped('PHPUnit will skip this test that will run only when app is running.');
        $response = new CustomResponse();
        $result = new Result("Ok", ["my_data"]);

        $responseCreated = $response->make($result);
        echo $responseCreated->content();
        $expected = "{\"data\":[\"my_data\"]}";
        $this->assertEquals($expected, $responseCreated->content());
    }
}