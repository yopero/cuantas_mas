<?php declare (strict_types = 1);
namespace Dgarrido\CuantasMasApi\PhpUnit;

use PHPUnit\Framework\TestCase as BaseTestCase;
use Dgarrido\CuantasMasApi\Models\Victim;
use Dgarrido\CuantasMasApi\Models\TsjCase;
use Dgarrido\CuantasMasApi\Models\PressRelease;
use Dgarrido\CuantasMasApi\Models\Location;
use Dgarrido\CuantasMasApi\Models\Assailant;

abstract class TestCase extends BaseTestCase
{
    public function hateoas(string $resourceName, int $id): array
    {
        return [
            'links' => [
                'self' => "http://localhost:9009/api/" . $resourceName . "/" . $id,
                'index' => "http://localhost:9009/api/" . $resourceName . "/",
            ],
        ];
    }

    public function getVictim(): Victim
    {
        $attributes = [
            "id" => 1,
            "femicideId" => 5,
            "name" => "Adrienne",
            "lastname" => "Bartoletti",
            "age" => 35,
            "previous_aggresions" => "ut sapiente",
            "children" => 9,
            "cause_of_death" => "debitis dolores doloremque",
            "created_at" => "2018-04-14 21:20:39",
            "updated_at" => "2018-04-14 21:20:39",
        ];
        $victim = new Victim();
        $victim->setRawAttributes($attributes);
        
        return $victim;
    }

    public function getTsjCase(): TsjCase
    {
        $attributes = [
            'id' => 47,
            'court_decision' => "Voluptatibus voluptatem sit non ipsa. Nemo sunt in tempora sint error. Officiis ducimus laudantium minus sunt incidunt et.",
            'date_decision' => "1999-03-12",
            'link' => "http://hermann.com/aperiam-nam-quos-nam-sit-ad-nostrum-natus-temporibus",
            "femicideId" => 5,
            "created_at" => "2018-04-14 21:20:39",
            "updated_at" => "2018-04-14 21:20:39",
        ];
        $tsj_case = new TsjCase();
        $tsj_case->setRawAttributes($attributes);
        
        return $tsj_case;
    }

    public function getPressRelease(): PressRelease
    {
        $attributes = [
            "id" => 1,
            "femicideId" => 5,
            "media_type" => "commodi",
            "link" => "http://rowe.com/",
            "created_at" => "2018-04-14 21:20:39",
            "updated_at" => "2018-04-14 21:20:39",
        ];
        $pressReleases = new PressRelease();
        $pressReleases->setRawAttributes($attributes);

        return $pressReleases;
    }

    public function getLocation(): Location
    {
        $attributes = [
            'id' => 47,
            'town' => "Port Nakiaberg",
            'province' => "Bessiemouth",
            'department' => "Hawaii",
            'lat' => "4.1570760",
            'lng' => "126.6622380",
            "femicideId" => 5,
            "created_at" => "2018-04-14 21:20:39",
            "updated_at" => "2018-04-14 21:20:39",
        ];
        $location = new Location();
        $location->setRawAttributes($attributes);

        return $location;
    }

    public function getAssailant(): Assailant
    {
        $attributes = [
            "id" => 1,
            "femicideId" => 5,
            "name" => "Enrique",
            "lastname" => "Bartoletti",
            "age" => 35,
            "temperance" => "ut sapiente",
            "suicide_attempt" => 0,
            "current_situation" => "debitis dolores doloremque",
            "relation_to_victim" => "magnam saepe",
            "created_at" => "2018-04-14 21:20:39",
            "updated_at" => "2018-04-14 21:20:39",
        ];
        $assailant = new Assailant();
        $assailant->setRawAttributes($attributes);

        return $assailant;
    }
}
