<?php

return [
    'api_url' => "https://" . env("API_URL", "localhost") . "/",
    'resources_display' => [
        'femicides'      => 'feminicidios',
        'assailants'     => 'asaltantes',
        'victims'        => 'victimas',
        'press_releases' => 'notas_de_prensa',
        'tsj_cases'      => 'casos_tsj',
        'locations'      => 'ubicaciones',
    ]
];