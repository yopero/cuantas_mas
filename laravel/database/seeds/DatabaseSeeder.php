<?php declare (strict_types = 1);

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        factory('Dgarrido\CuantasMasApi\Models\Femicide', 50)->create();
        factory('Dgarrido\CuantasMasApi\Models\Victim', 50)->create();
        factory('Dgarrido\CuantasMasApi\Models\Assailant', 50)->create();
        factory('Dgarrido\CuantasMasApi\Models\Location', 50)->create();
        factory('Dgarrido\CuantasMasApi\Models\PressRelease', 50)->create();
        factory('Dgarrido\CuantasMasApi\Models\TsjCase', 50)->create();
    }
}
