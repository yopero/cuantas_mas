<?php

use Faker\Generator as Faker;

$factory->define(Dgarrido\CuantasMasApi\Models\Victim::class, function (Faker $faker) {
    return [
        'femicideId' => $faker->unique()->numberBetween($min = 50, $max = 100) - 50,
        'name' => $faker->firstNameFemale,
        'lastname' => $faker->lastName,
        'age' => $faker->numberBetween(18,99),
        'previous_aggresions' => $faker->words($nb = 2, $asText = true),
        'children' => $faker->randomDigit,
        'cause_of_death' => $faker->words($nb = 3, $asText = true)
    ];
});
