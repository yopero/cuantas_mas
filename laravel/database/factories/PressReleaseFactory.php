<?php

use Faker\Generator as Faker;

$factory->define(Dgarrido\CuantasMasApi\Models\PressRelease::class, function (Faker $faker) {
    return [
        'femicideId' => $faker->unique()->numberBetween($min = 200, $max = 250) - 200,
        'media_type' => $faker->word,
        'link' => $faker->url,
    ];
});
