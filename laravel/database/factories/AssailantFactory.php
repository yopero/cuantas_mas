<?php

use Faker\Generator as Faker;

$factory->define(Dgarrido\CuantasMasApi\Models\Assailant::class, function (Faker $faker) {
    return [
        'femicideId' => $faker->unique()->numberBetween($min = 100, $max = 150) - 100,
        'name' => $faker->firstNameMale,
        'lastname' => $faker->lastName,
        'age' => $faker->numberBetween(18, 99),
        'temperance' => $faker->word,
        'suicide_attempt' => $faker->boolean(),
        'current_situation' => $faker->words($nb = 2, $asText = true),
        'relation_to_victim' => $faker->word,
    ];
});
