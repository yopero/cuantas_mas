<?php

use Faker\Generator as Faker;

$factory->define(Dgarrido\CuantasMasApi\Models\TsjCase::class, function (Faker $faker) {
    return [
        'femicideId' => $faker->unique()->numberBetween($min = 250, $max = 300) - 250,
        'court_decision' => $faker->paragraph(rand(2, 5), true),
        'date_decision' => $faker->date,
        'link' => $faker->url,
    ];
});
