<?php

use Faker\Generator as Faker;

$factory->define(Dgarrido\CuantasMasApi\Models\Location::class, function (Faker $faker) {
    return [
        'femicideId' => $faker->unique()->numberBetween($min = 150, $max = 200) - 150,
        'town' => $faker->city,
        'province' => $faker->city,
        'department' => $faker->state,
        'lat' => $faker->latitude($min = -90, $max = 90),  
        'lng' => $faker->longitude($min = -180, $max = 180),
    ];
});
